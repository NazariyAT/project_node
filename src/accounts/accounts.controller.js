const express=require('express');
const Account = require('./accounts.model');
const AccountServices = require('./accounts.service');
const AccountsController = express.Router();

AccountsController.get('/', async (req,res,next)=>{
    try{
        const accounts = await AccountServices.find()
        res.json(accounts)
    }catch(error){
        next(error)
    }
});

AccountsController.get('/:id', async (req,res,next)=>{
    try{
        const {id} = req.params;
        const account = await AccountServices.findOne(id)
        res.json(account)
    }catch(error){
        next(error)
    }
});

AccountsController.post('/', async (req,res,next)=>{
    try{
        const {name,characters} = req.body;
        const newAccount = await Account.create({name,characters});
        res.status(201).json(newAccount);
    }catch(error){
        next(error)
    }
});

AccountsController.put('/:id', async (req,res,next)=>{
    try{
        const {id} = req.params;
        const {name} = req.body;
        const update = await AccountServices.replace(id, {name})
        res.json(update)
    }catch(error){
        next(error)
    }
})

AccountsController.delete('/:id', async (req,res,next)=>{
    try{
        const {id} = req.params;
        await AccountServices.delete(id);
        res.status(204).send()
    }catch(error){
        next(error)
    }
})



module.exports=AccountsController;