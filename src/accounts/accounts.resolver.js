const AccountServices = require("./accounts.service");


class AccountResolver{
    static async existById(id){
        try{
            await AccountServices.findOne(id);
            return true;
        }catch(error){
            throw error;
        }
    }

    static async addCharactersToAccount(charId, accountId){
        const account = await AccountServices.findOne(accountId);
        account.characters ? account.characters.push(charId) : [charId];
        account.save();
    }
}

module.exports = AccountResolver;