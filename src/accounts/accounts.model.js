const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const account = {
    name:{type:String, required:true, trim:true},
    characters:[{type: mongoose.Types.ObjectId, ref:"Character"}]
};

const accountSchema = new Schema(account, {timestamps:true});
const Account = mongoose.model("Account", accountSchema)

module.exports=Account;

