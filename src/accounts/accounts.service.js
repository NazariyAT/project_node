const CharacterResolver = require('../characters/characters.resolver');
const StatusError = require('../_shared/error/status.error');
const Account = require('./accounts.model');

class AccountServices{
    static find(){
        return Account.find()
    }

    static async findOne(id) {
        const user = await Account.findById(id);
        if(user){
            return user;
        }

        throw new StatusError(404, `Account with id <${id}> was not found`);

    } 

    static async create(user){
        return Account.create(user)
    }

    static async replace(id, user){
        const updated= await Account.findByIdAndUpdate(id, user);
        if(updated){
            return updated;
        }

        throw new StatusError(404, `Account with id <${id}> was not found`);

    }

    static async delete(id){
        const user = await Account.findById(id);
        const characters = await CharacterResolver.findByAccount(id)
        if(characters.length){
            throw new StatusError(400, `Account has linked characters, remove them first before deleting user`);
        }
        if(user){
            return Account.findByIdAndRemove(id)
        }

        throw new StatusError(404, `Account with id <${id}> was not found`);

    }


}

module.exports=AccountServices;