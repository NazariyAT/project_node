const jwt = require("jsonwebtoken");

class JwtUtils {
  static generate(admin) {
    return jwt.sign(
      {
        id: admin._id,
        username: admin.username,
      },
      process.env.JWT_SECRET,
      { expiresIn: "1h" }
    );
  }

  static verify(token) {
    return jwt.verify(token, process.env.JWT_SECRET);
  }
}

module.exports = JwtUtils;
