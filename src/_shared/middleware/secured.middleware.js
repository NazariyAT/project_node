const AdminService = require("../../admin/admin.service");
const StatusError = require("../error/status.error");
const JwtUtils = require("../utils/jwt.utils");

const secured = async (req, res, next) => {
  try {
    const token = req.headers.authorization;

    if (!token) {
      throw new StatusError(401, "Unauthorized");
    }

    const parsedToken = token.replace("Bearer ","");
    const validToken = JwtUtils.verify(parsedToken, process.env.JWT_SECRET);
    req.user = await AdminService.findOne(validToken.id);
    next();

  } catch (error) {
    return next(error);
  }
};

module.exports = secured;
