//IMPORT
const express = require ('express');
const mongoose = require ('mongoose');
const CharactersController = require('./characters/characters.controller');
const AccountsController = require('./accounts/accounts.controller');
const AdminController = require('./admin/admin.controller');
const logging = require('./_shared/middleware/logging.middleware');
const {defaults} = require ('./_shared/utils/defaults.utils');
const secured = require('./_shared/middleware/secured.middleware');
const app = express();
require('dotenv').config();

//FORMAT+MIDDLEWARE
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(logging);


//ROUTES
app.use('/admin', AdminController)
app.use(secured)
app.use('/accounts', AccountsController)
app.use('/characters', CharactersController)

app.use('*', (req,res)=>res.status(404).json('Route does not exist'))

//ERROR
app.use((error,req,res,next)=>{
    const exception = {
        status: defaults(error.status, 500),
        messege: defaults(error.messege,'unexpected error')
    }
    
    if(process.env.NODE_ENV !== 'production'){
        exception['callstack'] = error.stack;
    }
    
    console.error(exception);
    res.status(exception.status).json(exception)
});

//CONECCION DB
const PORT = defaults(process.env.PORT, 3000);
mongoose.connect(process.env.MONGO_URI, {useNewUrlParser:true, useUnifiedTopology:true})
.then(()=>
app.listen(3000),()=>console.info(`Server is running in http://localhost:${PORT}`)
);