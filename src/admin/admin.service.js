const Admin = require("./admin.model");
const AdminResolver = require("./admin.resolver");
const bcrypt = require("bcrypt");
const JwtUtils = require("../_shared/utils/jwt.utils");

class AdminService {
  static find() {
    return Admin.find()/* .projection({ password: 0 }); */
  }

  static async findOne(id) {
    const admin = await Admin.findById(id)/* .projection({ password: 0 }); */
    if (admin) {
      return admin;
    }
  }

  static async create(admin) {
    const existsByEmail = await AdminResolver.existsByEmail(admin.email);
    if (existsByEmail) {
      throw new StatusError(400,`Admin with email <${admin.email}> already exists`
      );
    } 
    const hashedPassword = await bcrypt.hash(admin.password, 10);
    await Admin.create({ ...admin, password: hashedPassword });
  }

  static async login(admin) {
    const foundAdmin = await AdminResolver.findByEmail(admin.email);
    if (!foundAdmin) {
      throw new StatusError(403,`Admin with email <${admin.email}> does not exist`
      );
    }
    const isValidPassword = await bcrypt.compare(
      admin.password,
      foundAdmin.password
    );
    if (!isValidPassword) {
      throw new StatusError(403, `Wrong credentials`);
    }
    return {
      token: JwtUtils.generate(foundAdmin)
    };
  }

  static async replace(id, admin) {
    const updated = await Admin.findByIdAndUpdate(id, admin);

    if (updated) {
        return updated;
    }

    throw new StatusError(404, `Admin with id <${id}> was not found`);
}

  static async delete(id) {
    const admin = await Admin.findById(id);

    if (admin) {
        return Admin.findByIdAndRemove(id);
    }

    throw new StatusError(404, `Admin with id <${id}> was not found`);
}
}

module.exports = AdminService;
