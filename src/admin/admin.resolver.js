const Admin = require("./admin.model");

class AdminResolver {

  static existsByEmail(email) {
    return Admin.exists({ email });
  }

  static findByEmail(email) {
    return Admin.findOne({ email });
  }
}

module.exports = AdminResolver;
