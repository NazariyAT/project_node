const express = require("express");
const AdminService = require("./admin.service");
const AdminController = express.Router();
const secured = require('../_shared/middleware/secured.middleware')


AdminController.get("/",[secured], async (req, res, next) => {
    try {
      const admins = await AdminService.find();
      res.json(admins);
    } catch (error) {
      next(error);
    }
  }
);

AdminController.get("/:id", async (req, res, next) => {
    try {
      const { id } = req.params;

      const admin = await AdminService.findOne(id);

      res.json(admin);
    } catch (error) {
      next(error);
    }
  }
);

AdminController.post("/register", async (req, res, next) => {
  try {
    const { username, email, password } = req.body;

    await AdminService.create({ username, email, password });

    res.status(201).send();
  } catch (error) {
    next(error);
  }
});

AdminController.post("/login", async (req, res, next) => {
  try {
    const { email, password } = req.body;

    const created = await AdminService.login({ email, password });

    res.status(201).json(created);
  } catch (error) {
    next(error);
  }
});

AdminController.post("/logout", [secured], (req, res, next) => {
  try {
    const token = null
    res.status(201).json(token);
  } catch (error) {
    next(error);
  }
});

AdminController.put("/:id",[secured], async (req, res, next) => {
    try {
      const { username } = req.body;
      const { id } = req.params;

      const updated = await AdminService.replace(id, { username });

      res.json(updated);
    } catch (error) {
      next(error);
  }
});

AdminController.delete("/:id",[secured], async (req, res, next) => {
    try {
      const { id } = req.params;

      await AdminService.delete(id);

      res.status(204).send();
    } catch (error) {
      next(error);
  }
});

module.exports = AdminController;
