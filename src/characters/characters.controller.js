const express=require('express');
const CharacterServices = require('./characters.service');
const CharactersController = express.Router();

CharactersController.get('/', async (req,res,next)=>{
    try{
        const character = await CharacterServices.find()
        res.json(character)
    }catch(error){
        next(error)
    }
});

CharactersController.get('/:id', async (req,res,next)=>{
    try{
        const {id} = req.params;
        const character = await CharacterServices.findOne(id)
        res.json(character)
    }catch(error){
        next(error)
    }
});

CharactersController.post('/', async (req,res,next)=>{
    try{
        const {name,race,powerType,weaponType,account} = req.body;
        const newCharacter = await CharacterServices.create({name,race,powerType,weaponType,account});
        res.status(201).json(newCharacter);
    }catch(error){
        next(error)
    }
});

CharactersController.put('/:id', async (req,res,next)=>{
    try{
        const {id} = req.params;
        const {name,race,powerType,weaponType} = req.body;
        const fullUpdate = await CharacterServices.replace(id, {name,race,powerType,weaponType})
        res.json(fullUpdate)
    }catch(error){
        next(error)
    }
})

CharactersController.delete('/:id', async (req,res,next)=>{
    try{
        const {id} = req.params;
        await CharacterServices.delete(id);
        res.status(204).send()
    }catch(error){
        next(error)
    }
})



module.exports=CharactersController;