const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const character = {
    name:{type:String, required:true, trim:true},
    race:{type:String, required:true, trim:true, enum:["Human","Elf","Dwarf","Orc"]},
    powerType:{type:String, required:true, trim:true, enum:["Physical","Magic"]},
    weaponType:{type:String, required:true, trim:true, enum:["Sword","Dagger","Bow","Axe","Staff"]},
    account:{type: mongoose.Types.ObjectId, ref: "Account", required:true} 
};

const characterSchema = new Schema(character, {timestamps:true});
const Character = mongoose.model("Character", characterSchema)

module.exports=Character;

