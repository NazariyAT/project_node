const Character = require("./characters.model");

class CharacterResolver {
  static async findByAccount(id) {
    return Character.find({ account: id });
  }
}

module.exports = CharacterResolver;
