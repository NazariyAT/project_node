const StatusError = require('../_shared/error/status.error');
const Character = require('./characters.model');
const AccountResolver = require('../accounts/accounts.resolver')

class CharacterServices{
    static find(){
        return Character.find();
    }

    static async findOne(id) {
        const character = await Character.findById(id);
        if(character){
            return character;
        }

        throw new StatusError(404, `Character with id <${id}> was not found`);

    } 

    static async create(character){
        await AccountResolver.existById(character.account)
        const arrayChar = await Character.create(character)
        await AccountResolver.addCharactersToAccount(arrayChar._id ,character.account)
    }

    static async replace(id, character){
        const updated= await Character.findByIdAndUpdate(id, character);
        if(updated){
            return updated;
        }

        throw new StatusError(404, `Character with id <${id}> was not found`);

    }

    static async delete(id){
        const character = await Character.findById(id);
        if(character){
            return Character.findByIdAndRemove(id)
        }

        throw new StatusError(404, `Character with id <${id}> was not found`);

    }


}

module.exports=CharacterServices;